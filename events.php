<?php

$events = array(
  'fixemup' =>"Fix 'em Up" ,
  'engineerofthefuture' => "Engineer Of The Future",
  'techyhunt' =>"Techy Hunt" ,
  'junkyardwars' =>"Junkyard Wars" ,
  'paperpresentation' => "Paper Presentation",
  'waterrocketry' => "Water Rocketry",
  'sanrachana' => "Sanrachana",
  'paperplane' => "Paper Plane",
  'selfpropellingvehicle' => "Self Propelling Vehicle" ,
  'cadmodelling' => "CAD Modelling",
  'mcquiz' => "McQuiz"
);

$eventmembers = array(
  'fixemup' => 1,
  'engineerofthefuture' => 3,
  'techyhunt' => 1,
  'junkyardwars' => 5,
  'paperpresentation' => 2,
  'waterrocketry' => 3,
  'sanrachana' => 2,
  'paperplane' => 1,
  'selfpropellingvehicle' => 2,
  'cadmodelling' => 1,
  'mcquiz' => 2
);

$workshops = array(
  'automobile' => "Automobile Workshop",
  '3dprinting' => "3D Printer Workshop",
  'ornithopter' => "Ornithopter Workshop",
  'robotics' => "Robotics Workshop",
  'aeromodelling' => "Aero Modelling Workshop",
  'autocad' => "AutoCAD 2014 Workshop",
  'creo' => "Creo 2.0 Workshop",
  'solidworks' => "Solidworks 2014"
);

$workshopmembers = array(
  'automobile' => 1,
  '3dprinting' => 1,
  'ornithopter' => 2,
  'robotics' => 3,
  'autocad' => 1,
  'creo' => 1,
  'solidworks' =>1,
  'aeromodelling' => 1
);
$workshopfee = array(
  'automobile' => 650,
  '3dprinting' => 1290,
  'ornithopter' => 990,
  'robotics' => 500,
  'autocad' => 300,
  'creo' => 400,
  'solidworks' => 400,
  'aeromodelling' =>  950
);
$workshopslotes = array(
  'automobile' => array(14,15,16),
  '3dprinting' => array(15,16),
  'ornithopter' => array(15,16),
  'robotics' => array(15, 16),
  'autocad' => array(15, 16),
  'creo' => array(15, 16),
  'solidworks' => array(15, 16),
  'aeromodelling' =>  array(15, 16)
);
$workshopslotmaxmembers = array(
  'automobile' => array('14' => 100, '15' => 100, '16' => 100),
  '3dprinting' => array('15' => 100, '16' => 100),
  'ornithopter' => array('15' => 100, '16' => 100),
  'robotics' => array('15' => 100, '16' => 100),
  'autocad' => array('15' => 0, '16' => 100),
  'creo' => array('15' => 100, '16' => 100),
  'solidworks' => array('15' => 100, '16' => 100),
  'aeromodelling' =>  array('15' => 100, '16' => 100)
);
?>
